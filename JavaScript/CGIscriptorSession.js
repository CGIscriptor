/*
 *  LICENSE
 *  
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA  02111-1307, USA.
 * 
 *  Copyright 2012-2014 Rob van Son 
 *  email: R.J.J.H.vanSon@gmail.com 
 *  NKI-AVL Amsterdam
 */
// Global variables, set by the server as CGI parameter values
var CGIscriptorSessionType="<SCRIPT TYPE="text/ssperl" CGI='$SESSIONTYPE=""'>
	$SESSIONTYPE;</SCRIPT>";
var CGIscriptorChallengeTicket="<SCRIPT TYPE="text/ssperl" CGI='$CHALLENGETICKET=""'>
	$CHALLENGETICKET;</SCRIPT>";

var CGIscriptorLoginticket="<SCRIPT TYPE='text/ssperl' CGI='$LOGINTICKET=""'>
	$LOGINTICKET</SCRIPT>";
var CGIscriptorServerSalt="<SCRIPT TYPE='text/ssperl' CGI='$SERVERSALT=""'>
	$SERVERSALT</SCRIPT>";
var CGIscriptorRandomSalt="<SCRIPT TYPE='text/ssperl' CGI='$RANDOMSALT=""'>
	$RANDOMSALT</SCRIPT>";

// OnSubmit functions
function LoginSubmit () {
	var success=check_username_password();
	// Set the LOGINTICKET value in FORM
	var formID = document.getElementById("LOGINTICKET");
	var ipaddress = document.getElementById("CLIENTIPADDRESS").value;
	if(formID) {
		formID.value = CGIscriptorLoginticket;
	};
	
	SetSessionCookie();
	HashPassword(ipaddress+CGIscriptorRandomSalt);
	hidePasswords();
	return success;
};

function ChangePasswordSubmit () {
	if(! check_password_fields())return false;
	// Set the LOGINTICKET value in FORM
	var formID = document.getElementById("LOGINTICKET");
	if(formID) {
		formID.value = CGIscriptorLoginticket;
	};
	
	EncryptNewPassword("CGIUSERNAME");
	HashPassword(CGIscriptorRandomSalt);
	hidePasswords();
	return true;
};

function CreateUserSubmit () {
	if(! check_password_fields())return false;
	// Set the LOGINTICKET value in FORM
	var formID = document.getElementById("LOGINTICKET");
	if(formID) {
		formID.value = CGIscriptorLoginticket;
	};
	
	EncryptNewPassword("NEWUSERNAME");
	HashPassword(CGIscriptorRandomSalt);
	hidePasswords();
	return true;
};

// Function definitions
function hex_sha1 (plaintext) {
	var shaObj = new jsSHA(plaintext, "ASCII");
	return shaObj.getHash("SHA-1", "HEX");
};
function hex_sha256 (plaintext) {
	var shaObj = new jsSHA(plaintext, "ASCII");
	return shaObj.getHash("SHA-256", "HEX");
};
function hex_sha512 (plaintext) {
	var shaObj = new jsSHA(plaintext, "ASCII");
	return shaObj.getHash("SHA-256", "HEX");
};
function chained_sha (plaintext) {
	return hex_sha256( hex_sha256( hex_sha512(plaintext) ) );
};

function loadSessionData (SessionType, ChallengeTicket) {
	if(SessionType == 'CHALLENGE') 
		setChallengeParameters(ChallengeTicket);
	else if(SessionType == 'SESSION')
		setSessionParameters();
	return SessionType;
};

function createCookie(name,value,days,path) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	var match = document.cookie.match('/('+name+'\=[^\;]*\);/');
	if(match){
		while(match) {
			document.cookie = document.cookie.replace(match[1], name+"="+value);
			match = document.cookie.match('/('+name+'\=[^\;]*\);/');
		};
	} else {
		document.cookie = name+"=-";
		document.cookie = name+"="+value+expires+"; path=/"+path;
	};
};


function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
};

function eraseCookie(name) {
	createCookie(name,"",-1);
};

// Combine the PASSWORD with the site SERVERSALT and hash it
// Combine this Hash with the extra SERVERSALT, and hash them
function HashPassword(extsalt) {
	var hash = HashSessionSeed(extsalt);
	var password = document.getElementById('PASSWORD');
	if(password){
		password.value = hash;
	} else {
		alert("NO PASSWORD IN FORM");
		return 0;
	};
	return hash;
}

// REMEMBER: Set the session cookie BEFORE you hash the password!!!
function SetSessionCookie() {
	var loginticket = CGIscriptorLoginticket;
	var randomsalt = CGIscriptorRandomSalt;
	var hash = HashSessionSeed(loginticket);
	// Dom.storage.enabled must be set!
	if (!sessionStorage || typeof(sessionStorage) == 'undefined' ) {
		alert('Your browser does not support HTML5 sessionStorage. Set Dom.storage.enabled or try upgrading.');
		return 0;
	} 
	else sessionStorage.setItem("CGIscriptorPRIVATE", hash);
	
	// Store a secret key, if one is given
	SetSecretKey();
	
	return hash;
};

function SetSecretKey() {
	var loginticket = CGIscriptorLoginticket;
	var randomsalt = CGIscriptorRandomSalt;
	var secretkey = ""; 
	if (!sessionStorage || typeof(sessionStorage) == 'undefined' ) {
		alert('Your browser does not support HTML5 sessionStorage. Set Dom.storage.enabled or try upgrading.');
		return "";
	} 
	else if (loginticket && randomsalt) {
		secretkey = HashSessionSeed(loginticket+randomsalt);
		sessionStorage.setItem("CGIscriptorSECRET", secretkey);
	};
	
	return secretkey;
};

// Hash(hash(password+username.toLowerCase()+salt) + sessionseed)
function HashSessionSeed(sessionseed) {
	var hash1 = "";
	var hash2 = "";
	var passwordvalue = document.getElementById('PASSWORD');
	var saltvalue = CGIscriptorServerSalt;
	var username = document.getElementById('CGIUSERNAME');
 	hash1 = hex_sha256(passwordvalue.value+username.value.toLowerCase()+saltvalue);
 	if(sessionseed != "")
		hash2 = hex_sha256(hash1+sessionseed);
	else
		hash2 = hash1;
	return hash2;
}

// Remember to hash the repeat too! Or else it will be send in the clear
function HashNewPassword(userid) {
	var hash1 = "";
	var newpassword = document.getElementById('NEWPASSWORD');
	var newpasswordrep = document.getElementById('NEWPASSWORDREP');
	var username = document.getElementById(userid);
	if(newpassword.value == "" ) {
		newpassword.value = "";
		return 0;
	};
	if(newpasswordrep && (newpasswordrep.value == ""|| newpassword.value != newpasswordrep.value)) {
		newpassword.value = "";
		newpasswordrep.value = "";
		return 0;
	};
	var saltvalue = CGIscriptorServerSalt;
 	hash1 = hex_sha256(newpassword.value+username.value.toLowerCase()+saltvalue);
 	newpassword.value = hash1;
	newpasswordrep.value = hash1;
	return hash1;
};

function XOR_hex_strings(hex1, hex2) {
	var resultHex = "";
	var maxlength = Math.max(hex1.length, hex2.length);

	for(var i=0; i < maxlength; ++i) {
		var h1 = hex1.charAt(i);
		if(! h1) h1='0';
		var h2 = hex2.charAt(i);
		if(! h2) h2 ='0';
		var d1 = parseInt(h1,16);
		var d2 = parseInt(h2,16);
		var resultD = d1^d2;
		resultHex = resultHex+resultD.toString(16);
	};
	return resultHex;
};

function EncryptNewPassword(userid) {
	var newpassword = document.getElementById('NEWPASSWORD');
	var newpasswordrep = document.getElementById('NEWPASSWORDREP');
	var secretkey = SetSecretKey();
	
	// This hashes the newpassword field!
	HashNewPassword(userid);
	var encrypted = XOR_hex_strings(secretkey, newpassword.value);
 	newpassword.value = encrypted;
	newpasswordrep.value = encrypted;
	return encrypted;
};

function DecryptNewPassword(key, encrypted) {
	decrypted = XOR_hex_strings(key, encrypted);
	
	return decrypted;
};

function add_cgiparam(elem, attr, param) {
    var elems = document.getElementsByTagName(elem);
    for (var i = 0; i < elems.length; i++)
    {
		var n=elems[i][attr].indexOf("?");
		if(n<0)
			elems[i][attr] = elems[i][attr] + "?" + param;
		else
			elems[i][attr] = elems[i][attr] + "&" + param;
    };
};

function setSessionParameters() {
	var cgiScriptorPRIVATE = sessionStorage.getItem("CGIscriptorPRIVATE");
	// Use existing cookie
	if(! cgiScriptorPRIVATE) return true;
	
	var sessionset = readCookie("CGIscriptorSESSION");
	if(!(sessionset && sessionset.match(/[\S]/)))return false;

	var sessionticket = "";
	sessionticket = hex_sha256(cgiScriptorPRIVATE);
	sessionticket = hex_sha256(sessionticket+cgiScriptorPRIVATE);
	if(!sessionticket) return false;
	createCookie("CGIscriptorSESSION",sessionticket, 0, "");
	
 	// Without cookies, use this
	// var sessionparm = document.getElementById('SESSIONTICKET');
	// if(sessionparm) sessionparm.value = sessionticket;
    // add_cgiparam('a', 'href', "SESSIONTICKET="+sessionticket);
    // add_cgiparam('form', 'action', "SESSIONTICKET="+sessionticket);
    
    // UNCOMMENT for use in a local version of the Private/Login.html web page.
    // add_cgiparam('form', 'action', "SETCGISESSIONCOOKIE="+sessionticket);
    
	return true;
};
function setChallengeParameters(sessionset) {
	if(!(sessionset && sessionset.match(/[\S]/)))return false;
	
	var sessionticket = "";
	var sessionkey = sessionStorage.getItem("CGIscriptorPRIVATE");
	if(!sessionkey) return false;
	sessionticket = hex_sha256(sessionkey+sessionset);
	createCookie("CGIscriptorCHALLENGE",sessionticket, 0, "");

	// Without cookies, use this
	// var sessionparm = document.getElementById('CHALLENGETICKET');
	// if(sessionparm) sessionparm.value = sessionticket;
	
	// add_cgiparam('a', 'href', "CHALLENGETICKET="+sessionticket);
	// add_cgiparam('form', 'action', "CHALLENGETICKET="+sessionticket);
	return true;
};

function clear_persistent_data () {
	createCookie("CGIscriptorSESSION","", 0, "");
	createCookie("CGIscriptorCHALLENGE","", 0, "");
	sessionStorage.setItem("CGIscriptorPRIVATE", "");
	return true;
};

function check_password_fields ( ) {
	var newpassword = document.getElementById('NEWPASSWORD');
	var newpasswordrep = document.getElementById('NEWPASSWORDREP');
	if(newpassword.value == "" || newpasswordrep.value == "") {
		alert("No passwords");
		return false;
	};
	if(newpassword.value == newpasswordrep.value) {
		var submitbutton = document.getElementById('SUBMIT');
		submitbutton.style.color = "Black";
		return true;
	};
	alert("Passwords differ");
	return false;
};

function check_username_password ( ) {
	var username = document.getElementById('CGIUSERNAME');
	var password = document.getElementById('PASSWORD');
	if(username.value.match(/[a-zA-Z0-9]/) && password.value.match(/[a-zA-Z0-9]/))
		return true;
	alert("Please enter a user name and password");
	return false;
};

function revealPasswords () {
	var inputs = document.getElementsByTagName("input");
	for (i=(inputs.length-1); i>=0; i--) {
		var curr = inputs[i];
		if (curr.type.toLowerCase()=="password") {
			curr.type = "TEXT";
		};
	};
	
};

function hidePasswords () {
	var inputs = document.getElementsByTagName("input");
	for (i=(inputs.length-1); i>=0; i--) {
		var curr = inputs[i];
		if (curr.type.toLowerCase()=="text") {
			curr.type = "PASSWORD";
		};
	};
	
};

function togglePasswords (hide, show, value) {
	if(value.match(hide)) {
		hidePasswords ();
		return value.replace(hide, show);
	} else {
		revealPasswords ();
		return value.replace(show, hide);
	};
};

// Get a loginticket, salt, and random salt from a hidden loginFrame
// For use in a local version of the Private/Login.html web page.
// UNFINISHED WORK only useful with IPADDRESS sessions
// Put the following line in your local version of the HTML page to activate
// Replace http://localhost:8080/Private/index.html with the correct URL of the login page
// <iFrame id="loginFrame" src="http://localhost:8080/Private/index.html" hidden>Login frame</iFrame>

function getLoginData(){
	var frameID = document.getElementById("loginFrame");
	var iFrameHeader;
	var iFrameBody;
	if ( frameID.contentDocument ) 
	{ // FF
	  iFrameHeader = frameID.contentDocument.getElementsByTagName('head')[0];
	  iFrameBody = frameID.contentDocument.getElementsByTagName('body')[0];
	  iFrameHTML = frameID.contentDocument.getElementsByTagName('html')[0];
	}
	else if ( frameID.contentWindow ) 
	{ // IE
	  iFrameHeader = frameID.contentWindow.document.getElementsByTagName('head')[0];
	  iFrameBody   = frameID.contentWindow.document.getElementsByTagName('body')[0];
	  iFrameHTML   = frameID.contentWindow.document.getElementsByTagName('html')[0];
	}
	
	// Login ticket
	var myLoginexp = /CGIscriptorLoginticket="([a-f0-9]{64})"/;
	var myLogin = iFrameHeader.innerHTML.match(myLoginexp);
	if(myLogin) {
		CGIscriptorLoginticket = myLogin[1];
	} else {
		alert("Login not possible: Are you already logged in?\nLogin ticket missing");
	};	
	
	// Server Salt
	var mySaltexp = /CGIscriptorServerSalt="([a-f0-9]{64})"/;
	var mySalt = iFrameHeader.innerHTML.match(mySaltexp);
	if(mySalt){ 
		CGIscriptorServerSalt = mySalt[1];
	} else if(myLogin) {
		alert("Login not possible: Are you already logged in?\nServer salt missing");
	};	

	// Random Salt
	var myRandomexp = /CGIscriptorRandomSalt="([a-f0-9]{64})"/;
	var myRandom = iFrameHeader.innerHTML.match(myRandomexp);
	if(myRandom){ 
		CGIscriptorRandomSalt = myRandom[1];
	} else if(myLogin && mySalt) {
		alert("Login not possible: Are you already logged in?\nRandom salt missing");
	};	
	
	// REMOTE_HOST IP ADDRESS
	var myRemoteHostExp = /id=['"]*CLIENTIPADDRESS['"]* value=['"]([a-f0-9\.\:]+)['"]/;
	var myRemoteHost = iFrameBody.innerHTML.match(myRemoteHostExp);
	if(myRemoteHost){
		var ipaddress = document.getElementById("CLIENTIPADDRESS");
		ipaddress.value = myRemoteHost[1];
	};	
	
	// Clean out frame
	iFrameHTML.innerHTML = "<html><head><title>EMPTY</title></head><body><h1>EMPTY</h1></body></html>";
};

