/*
 *  LICENSE
 *  
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA  02111-1307, USA.
 * 
 *  Copyright 2012-2013 Rob van Son 
 *  email: R.J.J.H.vanSon@gmail.com 
 *  NKI-AVL Amsterdam
 */
<SCRIPT TYPE="text/ssperl" SRC="./JavaScript/CGIscriptorSession.js"></SCRIPT>

// Remove EVERYTHING from Login window
window.onload = function() {
	if(window.location.search)window.location.search = "";
	var warning = document.getElementById('WARNING');
	if(sessionStorage == null) warning.innerHTML = "Storage not supported by the browser: Upgrade your browser or set dom.storage.enabled";
	else {
		warning.style.color = "Black";
		warning.innerHTML = "";
	};
	clear_persistent_data ();
	
	// UNCOMMENT for use in a local version of the Private/Login.html web page.
	// getLoginData();
};

<SCRIPT TYPE="text/ssperl" SRC="./JavaScript/sha.js"></SCRIPT>
