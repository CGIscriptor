/*
 *  LICENSE
 *  
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA  02111-1307, USA.
 * 
 *  Copyright 2012-2013 Rob van Son 
 *  email: R.J.J.H.vanSon@gmail.com 
 *  NKI-AVL Amsterdam
 */
<SCRIPT TYPE="text/ssperl" SRC="./JavaScript/CGIscriptorSession.js"></SCRIPT>

window.onload = function() {
	loadSessionData (CGIscriptorSessionType, CGIscriptorChallengeTicket);
	// Handle SESSION login from a stored Login.html file
	<SCRIPT TYPE="text/ssperl" CGI="$SETCGISESSIONCOOKIE" if="$SETCGISESSIONCOOKIE=~m!^[0-9a-f]{64}$!" >
		"createCookie('CGIscriptorSESSION', '$SETCGISESSIONCOOKIE', 0, '');";
	</SCRIPT>
	return true;
};
 
<SCRIPT TYPE="text/ssperl" SRC="./JavaScript/sha.js"></SCRIPT>
