<META CONTENT="text/ssperl, CGI='$SERVER_ROOT $CGI_BINARY_FILE $REMOTE_ADDR'"><SCRIPT TYPE=text/ssperl>
# NOTE: no empty lines outside the script tags
# 
# 030300	Added directory access blocking for removing CVS directories from downloads
#
# This program contains two tricks:
# 1 .gz compressed and tar.gz compressed archive files are constructed on-line
#   when they don't exist. This is shut down on default for security reasons.
#   It is useful, but it is difficult to set up in a way that does not make 
#   each and every file in your web site available. Even hidden files.
# 2 URL: "http:/localhost:8080/<file1>*<file2>*...<fileN>.multipart" requests are treated  
#   as a single multi file 'multipart/mixed' MIME type request. That is, a single multipart
#   document with all requested files is constructed on-line (when it doesn't exist). 
#   If only the first file contains a URL path, the others will be prefixed with it, e.g.,
#   '/Audio/Daudio/AMDaudio/BLAMD001.WAV*BLAMD001.WAV.MULTIPART' is identical to
#   '/Audio/Daudio/AMDaudio/BLAMD001.WAV*/Audio/Daudio/AMDaudio/BLAMD001.WAV.MULTIPART'
#   NOTE: the view of multipart/mixed documents depends on the BROWSER used.
#
#
###############################################################################
#
# Author and Copyright (c):
# Rob van Son, � 1999,2000
# Institute of Phonetic Sciences & IFOTT/ACLC
# University of Amsterdam
# Email: R.J.J.H.vanSon@uva.nl
# WWW  : http://www.fon.hum.uva.nl/rob/
#
# License for use and disclaimers
#
# CGIscriptor merges plain ASCII HTML files transparantly  
# with CGI variables, in-line PERL code, shell commands, 
# and executable scripts in other scripting languages. 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
#
#######################################################>>>>>>>>>>Start Remove
#
# DIRECTORY ACCESS CONTROL: "foo|bar|thing" used as: =~ m@/($BlockAccess)/@
my $BlockAccess = "CVS";    # Block access to the CVS information

# Define mime types extension => mimetype (can be found in .mime.types file)
my %mimeType = (
'HTML' => "text/html",    # These are PROCESSED by CGIscriptor, not just written to STDOUT
'TXT'  => "text/plain",
'PL'  => "text/plain",    # This is incorrect, of course
'DIR'  => "text/html",    # This is incorrect, of course
#'CGI'  => "text/osshell", # Executing shell scripts should not be done
'JPG'  => "image/jpeg",
'JPEG' => "image/jpeg",
'GIF'  => "image/gif",
'TIFF' => "image/tiff",
'TIF'  => "image/tiff",
'AU'   => "audio/basic",
'AIF'  => "audio/aiff",
'AIFC' => "audio/aiff",
'AIFF' => "audio/aiff",
'WAV'  => "audio/wav",
'MPGA' => "audio/mpeg",
'MP2'  => "audio/mpeg",
'MPEG' => "video/mpeg",
'MPG'  => "video/mpeg",
'MPE'  => "video/mpeg",
'MULTIPART' => "multipart/mixed;boundary=NxtCntntTpZdFjgHk".time,  # use a '*' separated list
# 'GZ'   => "application/gzip"   # Constructs .gz and .tar.gz files on-line/run-time, dangerous
'QT'   => "video/quicktime",
'MOV'  => "video/quicktime"
);

$CGI_BINARY_FILE =~ /\.([\w]+)$/;	# Get extension
my $extension = uc($1);
$extension = 'DIR' if !$extension && -d "$SERVER_ROOT$CGI_BINARY_FILE";
unless(exists($mimeType{$extension})) # actually illegal mime
{
    exit 0;
};
my $mime = $mimeType{$extension};

my $String;
my $number_of_bytes; 

# Print the content type
$String = "Content-type: $mime\n\n";
$number_of_bytes = length($String); 
# Actually print the document header 
# (but NOT for OSSHELL scripts, which supply their own headers)
syswrite(STDOUT, $String, $number_of_bytes) unless $mime eq "text/osshell";

# THIS LINE PREVENTS ACCESS TO SPECIFIC PARTS OF THE DIRECTORY TREE
exit if $CGI_BINARY_FILE =~ m@/($BlockAccess)/@; # Prevent access to specific subtrees

# First, construct tar.gz and .gz files on-line
# Add "application/gzip" *only* if you have absolutely nothing to hide on your web site.
# It is very difficult to limit the download to only a part of the directory tree.
if($mime eq "application/gzip" &&  ! -e "$SERVER_ROOT$CGI_BINARY_FILE")
{
    # This is a gimmick to construct gnu-zipped tar-files from every requested directory at 
    # run-time This could be a security weakness and a CPU hog, so be carefull
    if($CGI_BINARY_FILE =~ m@\.tar\.gz$@is)
    {
		$SERVER_ROOT =~ m@/[^/]+$@;	# Get last directory name
		my $HomeDirectory = $&;		# The name of the current SERVER_ROOT dir
		my $PrePath = $`;			# The path tho the parent of the server root
		# THIS LINE PREVENTs SECURITY BREAKS: INSIST ON FULL PATHS STARTING AT SERVER-ROOT
		exit unless $CGI_BINARY_FILE =~ m@^$HomeDirectory@; # Prevent tricks
		
		$CGI_BINARY_FILE =~ m@\.tar\.gz$@is;	# Remove extensions (e.g., .tar.gz)
		my $TarPath = $PrePath.$`;		# The path to the directory to be TARRED
		die "Illegal tar request from $REMOTE_ADDR: $TarPath\n" unless -d $TarPath; # Only directories allowed
		
		# Use a pipe to prevent memory overloads (I hope). 
		# Note: the tar start at the PARENT directory of the directory to be
		# tarred. The tar excludes symbolic links and .log files.
		open(BINARY,
			"cd $PrePath;find $TarPath ! -type l ! -type d -print"
			."|grep -v '.log'|egrep -v '/$BlockAccess/'"
			."|sed 's\@$PrePath\@.\@g'|tar -cf - -T -|gzip|")  # Linux tar
		|| open(BINARY,
			"cd $PrePath;find $TarPath ! -type l ! -type d -print"
		    ."|grep -v '.log'|egrep -v '/$BlockAccess/'"
			."|sed 's\@$PrePath\@.\@g'|tar -cf - -|gzip|")     # SGI Irix tar
		|| die "$!\n";
		# If you want to tar WHOLE directories unconditionally, uncomment the 
		# following (and remove the above)
		# open(BINARY, "cd $PrePath;tar -cf - .$HomeDirectory|gzip|");
		
		# read and write block of 1024 bytes
		while($number_of_bytes = sysread(BINARY, $String, 1024))
		{
		    syswrite(STDOUT, $String, $number_of_bytes); # Actually print the file content
		};
	    close(BINARY);
    }
    # This will construct a gnu-zipped file from every file requested: GET foo.bar.gz will
    # result in foo.bar being zipped before delivery.
    else
    {
		$CGI_BINARY_FILE =~ m@\.gz$@is;	# Remove extensions (e.g., .tar.gz)
		my $GzipPath = $SERVER_ROOT.$`;        # The path to the file to be zipped
	
		# Limit gzip activity to supported file-types
		$GzipPath =~ /\.([\w]+)$/;	# Get extension
        my $GzipExtension = uc($1);
        my $GzipMime = $mimeType{$GzipExtension}; # mime of file to be zipped
		die "Illegal tar request from $REMOTE_ADDR: $GzipPath\n" unless $GzipMime && ($GzipMime ne $mime);
		
		# Use a pipe to prevent memory overloads (I hope)
		open(BINARY, "gzip -c $GzipPath|");
		# read and write block of 1024 bytes
		while($number_of_bytes = sysread(BINARY, $String, 1024))
		{
		    syswrite(STDOUT, $String, $number_of_bytes); # Actually print the file content
		};
        close(BINARY);
    };  
}
# Second, trick to handle 'multipart/mixed', ie, combined pages, on-line.
# This allows the playback of a sound file coupled to the display of an 
# HTML file
elsif($mime =~ m@^multipart/mixed@isg &&  ! -e "$SERVER_ROOT$CGI_BINARY_FILE")
{
    $CGI_BINARY_FILE =~ m@[^/]+$@;
    my $URLpath = $`;
    my $BinaryFileList = $&;
    if($URLpath =~ /\*/)   # If each file has separate path, do not use common URLpath
    {
	$BinaryFileList = $CGI_BINARY_FILE;
	$URLpath = '';
    };

    $BinaryFileList =~ s/\.$extension$//isg;  # Remove Multipart extension
    my @FileList = split('\*', $BinaryFileList);
    my $InputFile;
    $mime =~ /\;boundary\=/;
    my $MultipartSeparator = $';
    foreach $InputFile (@FileList)
    {
	
       # Write separation string
       my $String = "\n--$MultipartSeparator\n";
       my $number_of_bytes = length($String);
       syswrite(STDOUT, $String, $number_of_bytes); # Actually print the separation string

       # Recursively process file parts
       $CGI_BINARY_FILE = $URLpath.$InputFile;
       $ENV{'CGI_BINARY_FILE'} = $URLpath.$InputFile;
       main::ProcessFile("~/BinaryMapFile.xmr");
    };
    # Write closing separation string
    my $String = "\n--$MultipartSeparator\n";
    my $number_of_bytes = length($String);
    syswrite(STDOUT, $String, $number_of_bytes); # Actually print the separation string
       
}
# Third, process HTML files as CGIscriptor HTML files (when they are part of MULTIPART/MIXED files)
elsif($mime eq 'text/html')
{
    main::ProcessFile("~/$CGI_BINARY_FILE") unless -d "$SERVER_ROOT$CGI_BINARY_FILE";
    CGIscriptor::BrowseAllDirs($CGI_BINARY_FILE, 'index.html') if -d "$SERVER_ROOT$CGI_BINARY_FILE";
}
# If you realy cannot live without shell scripts (note the ./)
# Note that the script must supply the content type!
elsif($mime eq 'text/osshell')
{
    # Check filename for safety
    die "./".$CGI_BINARY_FILE." from $REMOTE_ADDR\n" 
	unless CGIscriptor::CGIsafeFileName($CGI_BINARY_FILE); 
    # This doesn't "feel" safe, but it works
    print STDOUT `./$CGI_BINARY_FILE`;
}
else  # All other files are just printed
{
    open(BINARY, "<$SERVER_ROOT$CGI_BINARY_FILE") || die "$SERVER_ROOT$CGI_BINARY_FILE: $!";
    
    # read and write block of 1024 bytes
    while($number_of_bytes = sysread(BINARY, $String, 1024))
    {
	    syswrite(STDOUT, $String, $number_of_bytes); # Actually print the file content
    };
    close(BINARY);
};

"";
</SCRIPT>
