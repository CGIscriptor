Version 2.3 (15 Jan 2002)
CGIscriptor.pl and CGIservlet.pl (zipped tar file) 

License:
All programs are subject to the GNU Public License (GPL) version 2 or later. 
The text of the GPL is included in the distribution.

NOTE: Be sure to read the disclaimer in the GPL license and inside 
the perl scripts. Executing any of the scripts implies that you have
agreed to these disclaimers in full.

Rob van Son
R.J.J.H.vanSon@uva.nl

Security:
Using these programs can pose security risks. They should be 
used with care, preferentially not on mission-critical systems or 
systems containing sensitive data. In case of doubt, run CGIservlet.pl
with the -r option (i.e., restrict access to localhost).
Note the disclaimer in the GPL license.

Requirements:
CGIscriptor.pl requires perl 5.000, CGIservlet.pl requires perl 5.002.  
CGIservlet needs full socket support.  Currently, CGIservlet expects 
a perl link in the startup directory (or a full path on the first line of 
CGIservlet.pl).  The supplied StartServlet script will create a link to 
perl if it doesn't exist.

Unpacking:
> tar -xzf CGIscriptor.tar.gz

########################################################################

CGIscriptor.pl is a HTML 4 compliant script/module for integrating HTML, 
CGI, and perl scripts at the server side.  

CGIservlet.pl is a pocket-size HTTP daemon that will run a single script.  

Together with CGIscriptor.pl and a special file (BinaryMapFile.xmr), 
CGIservlet.pl will make a complete WWW-server that can fit a running 
Web-site on a single floppy. 

Although this �-server distribution itself is of some use (to me at least), 
it is only included as a demonstration of CGscriptor.pl.  Previous 
experience has shown me that most prospective users had difficulties 
accessing and configuring their web-servers.  Although adding CGIscriptor.pl 
to an Apache web-server amounts to adding a few directory paths to 
CGIscriptor.pl itself and only a single ScriptAlias line to the srm.conf 
file of Apache (and a HUP), even this seemed to be too intimidating.  
Therefore, this package runs out-of-the-box as a micro "web-site" to
allow some experimenting before adding it to the "real" server.

########################################################################

After unpacking the GNUzipped TAR file, the WWW server can be started 
with only three commands 
(sh, use your own path to perl 5.002 or higher, if necessary):

> cd CGIscriptor; ln -s /bin/perl perl; CGIservlet.pl 2>pid.log& 

or

> cd CGIscriptor; StartServlet
(StartServlet is an sh script that combines the last two commands)

It can be stopped with:
> sh pid.log 
or
> StopServlet

point your browser to URL:
http://localhost:8080/
or
http://localhost:8080/Hello.html

If port 8080 is already in use on your platform, change the command to (sh):
> cd CGIscriptor; ln -s /bin/perl perl; CGIservlet.pl -p 2345 2>pid.log& 
and use URL:
http://localhost:2345/
(or use any other convenient port number to replace 2345, you can also
change StartServlet or the default setting inside CGIservlet.pl)

The package is configured to run "out-of-the-box" but can be fully 
configured to fit your personal environment.

Note:
All programs contain their documentation as comments. Please read them.
At the end of this file there is some more information on the 
installation procedure.

#######################################################################

The current default setting of CGIservlet.pl implements an interactive 
WWW server using the CGIscriptor package and the BinaryMapFile.xmr page.
These settings correspond to:
 -p 8080 
 -t `pwd` 
 -b 'require "CGIscriptor.pl";' 
 -e 'HandleRequest();'
 -d ''
 -w '/index.html'
 -c 32
 -l 32767

########################################################################

The directory CGIscriptor contains:
- Directories
JavaScript
  CGIscriptorSession.js - Example JavaScript code for login sessions
  SecureHash.js - Cryptographic Secure Hash code for login sessions
Private
  .Passwords    - Directory with server SALT and account information
  .Sessions     - Directory to store the Session Tickets
  ChangePassword.html - Example HTML file for password change
  Login.html    - Example HTML file for login page
  index.html    - Example HTML file
  manual.html   - Example HTML file


- Files
BinaryMapFile.xmr
CGIscriptor.html
CGIscriptor.pl
CGIservlet.html
CGIservlet.pl
CGIservletSETUP.pl
Hello.html
README.txt
StartServlet
StopServlet
gpl.html
gpl.txt
index.html
philosophical-gnu-sm.jpg
test.html

- StartServlet & StopServlet
Two sh shell scripts to start and stop the servlet. The stop command 
and errors are written to pid.log. StartServlet creates a link to the
perl interpreter if it doesn't exist.

- CGIservlet.pl
A pocket-size HTTP daemon. The Internet connection for a WWW script or 
server.

- CGIscriptor.pl
A CGI-aware HTML processor with server-side perl and shell script 
execution to serve up integrated HTML/Perl/CGI forms and documents.  
Works with any WWW-server that can execute perl scripts.  Together 
with CGIservlet.pl and BinaryMapFile.xmr this makes a full-fledged 
WWW-server.

- BinaryMapFile.xmr
A special CGIscriptor.pl document that will serve binary files 
(CGIscriptor.pl only serves text-files). This file contains
plain Perl code with a CGI connection. 
Currently, it implements two on-line 'tricks': BinaryMapFile.xmr
will create on-line compressed files (.gz) and compressed archives
(.tar.gz) whenever the appropriate extension is seen, e.g.,
CGIscriptor.pl.gz will result in a GNU-zipped version of CGIscriptor.pl,
and CGIscriptor.tar.gz will result in a compressed tar file of the whole
CGIscriptor directory (stops at the $SERVER_ROOT directory).
The other trick is that 'multipart/mixed' documents are created
when a *-separated list of files is ended with the '.multipart' extension.
For example: http://localhost:8080/audio.wav*Hello.html.multipart
would result in a single multipart document being served containing 
both 'audio.wav' and 'Hello.html'.

- CGIscriptor.html
The CGIscriptor manual of CGIscriptor.pl (i.e., the comments) in
HTML format.

- CGIservlet.html
The CGIservlet manual of CGIservlet.pl (i.e., the comments) in
HTML format.

- Hello.html & test.html
A sample integrated HTML/Perl/CGI document for CGIscriptor.pl.

- index.html
A server-root home page for CGIservlet/CGIscriptor

- gpl.html
  gpl.txt
  philosophical-gnu-sm.jpg
The GPL license and an attached GNU picture.

- README.txt
This file.

#######################################################################

Installation:
CGIscriptor.pl and CGIservlet.pl will come configured to run perl, or a 
link to it, from the "current" working directory (`pwd`). Change the 
first line in the files to point to your perl distribution if you want
it otherwise.

CGIscriptor.pl can be added to the script directory of your WWW server.  
Default, CGIscriptor.pl will serve documents from the SERVER_ROOT directory 
tree.  You can point it to any other directory (see the text inside 
CGIscriptor.pl).

If your perl distribution (5.002 or higher) is configured to use sockets, 
you can start the demo version by entering:

> cd CGIscriptor

perform once:
> ln -s /bin/perl perl          # a link to perl 5.002 is necessary

select one of:
> CGIservlet.pl 2>pid.log&      # SH
> CGIservlet.pl >&pid.log&      # CSH

or alternatively
> cd CGIscriptor
> StartServlet                  # combines the 'ln -s' and 'CGIservlet' commands

Pointing your browser to URL:
http://localhost:8080/Hello.html
should result in a page saying "Hello World" and displaying some HTTP 
related parameters.
URL: http://localhost:8080/ gives a WWW vision on this distribution.

The file pid.log contains a 'kill -9' command and the process number of 
the servlet on the first line. To stop the servlet use:
> sh pid.log
or
> StopServlet

All requests are logged in the file "Client.log".

NOTE: You can also run CGIscriptor.pl from the command line with the file path 
and query string as arguments. The resulting text will be send to STDOUT, 
e.g.:
> CGIscriptor.pl 'Hello.html' 'Input=Get Out' 'Button=3'
will print a lot of HTML text to your screen.
