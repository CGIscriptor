# Configure CGIservlet by assigning to the relevant variables.
# This is just a copy of the default setup area of CGIservlet.pl.
# This file is read AFTER the default setup, but BEFORE the command line 
# options are processed.
#
# License for use and disclaimers
#
# CGIscriptor merges plain ASCII HTML files transparantly  
# with CGI variables, in-line PERL code, shell commands, 
# and executable scripts in other scripting languages. 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
############################################################
#                                                          #
#   Parse arguments (you can define DEFAULT VALUES here)   #
#                                                          #
############################################################

# User set Envrionment variables

# Master password
$UserEnv{'CGIMasterKey'} = "Sherlock investigates oleander curry in Bath";
#$REQUEST_CGIMASTERKEY_FROM_TERMINAL = 1;

# In real use, you should read the key from a keyfile (on USB) and do a 
# SHA256(plaintext_key) if you have to use readable text!, e.g.,
# open(KEYFILE, "</media/KEYS/keyfile");$UserEnv{'CGIMasterKey'} = <KEYFILE>;chomp($UserEnv{'CGIMasterKey'});close(KEYFILE);

$port      = 8080;	     # The port number

# (Fast) direct translation of full URL paths
%AliasTranslation = ('/Welcome.html' => '/');   # Alias => RealURL pairs (ONLY paths)
# Regular expression alias translation, in order of application
# (this can be quite slow)
@RegAliasTranslation = ('^(\..*|.*/\..*)$','\.htm$', '^.*\.log$'); # Full regular expression alias/url pairs: URL
@RegURLTranslation = ('/','.html', '/');   # Full regular expression alias/url pairs: PATH

#$textroot  = $ENV{'PWD'} || `pwd`; # current working directory 
#chomp($textroot);	     # Remove nasty newline, if present
#$doarg     = '';          # do "filename", 

$beginarg  = "require 'CGIscriptor.pl';"; # eval($Argument) at the start of the program
# Use the files from CGIscriptor/ if it exists
$beginarg  = "require 'CGIscriptor/CGIscriptor.pl'" if -d 'CGIscriptor'; 

$evalarg   = 'Handle_Request();'; # eval($Argument) for each request
$execarg   = '';          # `command \'$textroot$Path\' \'$QueryString\'`

#$welcome   = '/index.html';  # Default path

#  Rudimentary security, overflow detection
#$MaxBrood  = 32;		# Maximum number of running children
#$MaxTime  = 36000;           # Maximum time a child may run in seconds
#$MaxLength = 2**15;          # Maximum Request Length 
#$UseFAT = 0;	# Run on FAT systems (Windows) such as thumb drives (default: NO)

# If one of the following lists contains any client addresses or names, all others are
# blocked (be carefull, your site will be inaccessible if you misspell them).
#@RemoteHost = ();		# Accepted Hosts, suggest: localhost
#@RemoteAddr = ();		# Accepted IP addresses, suggest: @RemoteAddr=('127.0.0.1')
#$DefaultRemoteAddr = '127.0.0.1';  # default, use localhost IP address
#$NONAME = 0;			# if 1, do NOT ask for REMOTE_HOST (faster)

# Store the whole Web Site in a hash table and use this RAM image
$UseRAMimage = 0;

# Execute shell CGI scripts when no -d, -e, or -x are supplied
$ExecuteOSshell = 0;		# Do you REALY want this? It is dangerous

1;	# Make require happy
